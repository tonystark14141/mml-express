import { Component, OnInit } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ToastrManager } from 'ng6-toastr-notifications';
import { PopupManager } from 'ng6-popup-boxes';
import { MasterServiceService } from '../../services/master-service.service'

@Component({
  selector: 'app-masters',
  templateUrl: './masters.component.html',
  styleUrls: ['./masters.component.scss']
})
export class MastersComponent implements OnInit {
	branchlist:any;
	customerlist:any;
  id:any;
	tarifflist:any;
	origin_id_by_customer:any;
	customer_id:any;
	serviceList:any;
	paymentList:any;
	shipmentList:any;
  rateTypeList:any;
  fileURL:any;
  rateListEdit={
    customer_rate_id: undefined,
    customer_name: undefined,
    destination_name: undefined,
    service_type: undefined,
    shipment_type: undefined,
    minimum_wgt: undefined,
    maximum_wgt: undefined,
    min_rate_per_shipment: undefined,
    transit_days_to_delivery: undefined,
    fuel_sur_charge: undefined,
    flat_rate_per_kg_above_maximum: undefined,
    rate_calculation_method: undefined,
    wh_storage_location_name: undefined,
    transit_type_name: undefined,
    document_type_name: undefined,
    payment_mode: undefined,
    client_name: undefined,
    client_code: undefined
}
  insertList:any;
  insertSlap={
    customer_rate_master:undefined,
      slap_weight:undefined,
      slap_amount:undefined,
      slap_rate_type_UOM:undefined
  }
  updateList={
    customer_rate_id:undefined,
      customer_name:undefined,
      destination_name:undefined,
      service_type:undefined,
      shipment_type:undefined,
      payment_mode:undefined,
      minimum_wgt:undefined,
      maximum_wgt:undefined,
      min_rate_per_shipment:undefined,
      transit_days_to_delivery:undefined,
      fuel_sur_charge:undefined,
      flat_rate_per_kg_above_maximum:undefined,
      rate_calculation_method:undefined,
      wh_storage_location_name:undefined,
      transit_type_name:undefined,
      document_type_name:undefined,
      client_name:undefined,
      client_code:undefined
  }
	rateList={
		customer_name:"",
		wh_storage_location_name:"",
		payment_mode:"",
		transit_type_name:"",
		shipment_type:"",
		minimum_wgt:"",
		min_rate_per_shipment:"",
		transit_days_to_delivery:"",
		fuel_sur_charge:"",

	}
  slapListget:any;
  slapList={
    customer_slap_rate_id:undefined,
    customer_rate_master:undefined,
    slap_weight:undefined,
    slap_amount:undefined,
    slap_rate_type_UOM:undefined
  }
  slapUpdate={
    customer_slap_rate_id: undefined,
    customer_rate_master: undefined,
    slap_weight: undefined,
    slap_amount: undefined,
    slap_rate_type_UOM: undefined
  }
	destinationList:any;

  constructor(public http:HttpClientModule, public service:MasterServiceService,
   public toastr: ToastrManager, public popupManager: PopupManager) { }

  branchList(){
  	this.service.get_branch().subscribe(branch=>{
  		this.branchlist=branch['data'];
  		console.log("or",this.branchlist);
  	})
  }
  customerList(origin_id_by_customer){
  	this.service.get_customer(origin_id_by_customer).subscribe(customer=>{
  		this.customerlist=customer['data'];
  	})
  }
  tariffList(customer_id){
    this.rateList.customer_name=customer_id;
  	this.service.get_tariff(customer_id).subscribe(tariff=>{
  		this.tarifflist=tariff['data'];
      console.log("tariff",this.tarifflist);
  	})
  }
  destination(){
  	this.service.get_destination().subscribe(des=>{
  		this.destinationList=des['data'];
  	})
  }
  getService(){
  	this.service.get_service().subscribe(service=>{
  		this.serviceList=service['data'];
  	})
  }
  getPayment(){
  	this.service.get_payment().subscribe(payment=>{
  		this.paymentList=payment['data'];
  	})
  }
  getShipment(){
  	this.service.get_shipment_type().subscribe(shipment=>{
  		this.shipmentList=shipment['data'];
  	})
  }
  edit(id){
  	this.service.get_rate_id(id).subscribe(rate=>{
  		this.rateListEdit=rate['data'][0];

  		console.log("rateListEdit ",this.rateListEdit);
      // this.insertSlap.customer_rate_master=rate['data'][0]['customer_rate_id'];

//   ngOnInit() {
//   	let sathish={
//   		"cars": {
//         "Nissan": [
//             {"model":"Sentra", "doors":4},
//             {"model":"Maxima", "doors":4},
//             {"model":"Skyline", "doors":2}
//         ],
//         "Ford": [
//             {"model":"Taurus", "doors":4},
//             {"model":"Escort", "doors":4}
//         ]
//     }
// }
// 	console.log(sathish);
// this.car=sathish['cars']
// =======
//       this.rateList.customer_name=this.rateListEdit.customer_name;
//       this.rateList.wh_storage_location_name=this.rateListEdit.destination_name;
//       this.rateList.payment_mode=this.rateListEdit.payment_mode;
//       this.rateList.transit_type_name=this.rateListEdit.service_type;
//       this.rateList.shipment_type=this.rateListEdit.shipment_type;
//       this.rateList.minimum_wgt=this.rateListEdit.minimum_wgt;
//       this.rateList.min_rate_per_shipment=this.rateListEdit.min_rate_per_shipment;
//       this.rateList.transit_days_to_delivery=this.rateListEdit.transit_days_to_delivery;
//       this.rateList.fuel_sur_charge=this.rateListEdit.fuel_sur_charge;
      // this.updateList.customer_rate_id=this.rateListEdit.customer_rate_id;
      // this.updateList.maximum_wgt=this.rateListEdit.maximum_wgt;
      // this.updateList.flat_rate_per_kg_above_maximum=this.rateListEdit.flat_rate_per_kg_above_maximum;
      // this.updateList.rate_calculation_method=this.rateListEdit.rate_calculation_method;
      // this.updateList.wh_storage_location_name=this.rateListEdit.wh_storage_location_name;
      // this.updateList.document_type_name=this.rateListEdit.document_type_name;
      // this.updateList.client_name=this.rateListEdit.client_name;
      // this.updateList.client_code=this.rateListEdit.client_code;
      // this.updateList.transit_type_name=this.rateListEdit.transit_type_name;
  // 	})
  // 	this.service.get_slap_id(id).subscribe(slap=>{
  // 		this.slapListget=slap['data'];
  //     console.log("slapListget ",this.slapListget);

  	})
  }
  edit_slap(id){
    this.service.get_slap_id_get(id).subscribe(slap_get=>{
      this.slapList=slap_get['data'][0];
      console.log("slap Edit slapList",this.slapList)
    })
  }
  getRate(){
    this.service.get_rate().subscribe(rate=>{
      this.rateTypeList=rate['data'];
    })
  }
  insert(){
    this.insertList={
      customer_name:this.rateList.customer_name,
      destination_name:this.rateList.wh_storage_location_name,
      service_type:this.rateList.transit_type_name,
      shipment_type:this.rateList.shipment_type,
      payment_mode:this.rateList.payment_mode,
      minimum_wgt:this.rateList.minimum_wgt,
      min_rate_per_shipment:this.rateList.min_rate_per_shipment,
      transit_days_to_delivery:this.rateList.transit_days_to_delivery,
      fuel_sur_charge:this.rateList.fuel_sur_charge,
  }
    console.log("ins",this.insertList);
    this.service.insert(this.insertList).subscribe(ins_res=>{
      if (ins_res['CODE'] == '200') {
        this.tariffList(this.customer_id);
        this.toastr.successToastr('insert success','success');
      }
    })
  }
  update(){

      this.updateList.customer_rate_id=this.rateListEdit.customer_rate_id;
      this.updateList.customer_name=this.rateList.customer_name;
      this.updateList.destination_name=this.rateList.wh_storage_location_name;
      this.updateList.service_type=this.rateList.transit_type_name;
      this.updateList.shipment_type=this.rateList.shipment_type;
      this.updateList.payment_mode=this.rateList.payment_mode;
      this.updateList.minimum_wgt=this.rateList.minimum_wgt;
      this.updateList.maximum_wgt=this.rateListEdit.maximum_wgt;
      this.updateList.min_rate_per_shipment=this.rateList.min_rate_per_shipment;
      this.updateList.transit_days_to_delivery=this.rateList.transit_days_to_delivery;
      this.updateList.fuel_sur_charge=this.rateList.fuel_sur_charge;
      this.updateList.flat_rate_per_kg_above_maximum=this.rateListEdit.flat_rate_per_kg_above_maximum;
      this.updateList.rate_calculation_method=this.rateListEdit.rate_calculation_method;
      this.updateList.wh_storage_location_name=this.rateListEdit.wh_storage_location_name;
      this.updateList.transit_type_name=this.rateListEdit.transit_type_name;
      this.updateList.document_type_name=this.rateListEdit.document_type_name;
      this.updateList.client_name=this.rateListEdit.client_name;
      this.updateList.client_code=this.rateListEdit.client_code;
    this.service.update(this.updateList).subscribe(res_update=>{
      if (res_update['CODE']=='200') {
        this.toastr.successToastr('update sucess', 'success');
        this.tariffList(this.customer_id);
      }
    })
  }
  insert_slap(){
    console.log(" insert this.rateListEdit ",this.rateListEdit);
    this.insertSlap.customer_rate_master=this.rateListEdit.customer_rate_id;
    this.insertSlap.slap_weight=this.slapList.slap_amount;
    this.insertSlap.slap_amount=this.slapList.slap_weight;
    this.insertSlap.slap_rate_type_UOM=this.slapList.slap_rate_type_UOM;
    console.log("salp",this.insertSlap);
    this.service.insert_slap(this.insertSlap).subscribe(res_slap=>{
      if (res_slap['CODE']=='200') {
        this.toastr.successToastr('Insert success','SUCCESS');
        this.edit(this.rateListEdit.customer_rate_id);
        this.reset_slap();
      }
    })
  }
  update_slap(){
    this.slapUpdate.customer_slap_rate_id=this.slapList.customer_slap_rate_id;
    this.slapUpdate.customer_rate_master=this.slapList.customer_rate_master;
    this.slapUpdate.slap_weight=this.slapList.slap_weight;
    this.slapUpdate.slap_amount=this.slapList.slap_amount;
    this.slapUpdate.slap_rate_type_UOM=this.slapList.slap_rate_type_UOM;
    this.service.update_slap(this.slapUpdate).subscribe(res_update=>{
      console.log("update_slap",this.slapUpdate);
      if (res_update['CODE'] =='200') {
        this.toastr.successToastr('update Success','SUCCESS');
        this.edit(this.rateListEdit.customer_rate_id);
        this.reset_slap();
      }
    })
  }
  deleteTariff(id){
    this.popupManager.open('Delete', 'Do you really want to this item?', 
      {
            width: '300px',
            closeOnOverlay: false,
            animate: 'scale',
            actionButtons: 
            [
              {
                text: 'Yes',
                buttonClasses: 'btn-ok',
                onAction: () =>
                {
                this.service.delete_tariff(id).subscribe(res_del=>{
                  if (res_del[0]['CODE']=='200') {
                    this.tariffList(this.customer_id);
                    this.toastr.successToastr('Delete Success','Deleted');
                  }
                })
                return true;
                }
              },
              {
                text: 'No',
                buttonClasses: 'btn-cancel',
                onAction: () => {
                  return false;
                }
              }
            ],
          
          });
  }
  delete_slap(id){
    this.popupManager.open('Delete', 'Do you really want to this item?', 
      {
            width: '300px',
            closeOnOverlay: false,
            animate: 'scale',
            actionButtons: 
            [
              {
                text: 'Yes',
                buttonClasses: 'btn-ok',
                onAction: () =>
                {
                this.service.delete_slap(id).subscribe(res_del=>{
                  if (res_del[0]['CODE']=='200') {
                    this.toastr.successToastr('Delete Success','Success');
                    this.edit(this.rateListEdit.customer_rate_id);
                  }
                })
                return true;
                }
              },
              {
                text: 'No',
                buttonClasses: 'btn-cancel',
                onAction: () => {
                  return false;
                }
              }
            ],
          
          });
  }
  print(){
    this.service.print(this.customer_id).subscribe(res_url=>{
      this.fileURL = res_url['file_url'];
      window.open(this.fileURL, '_blank');
    })
  }
  reset(){
    this.rateList.customer_name=undefined;
    this.rateList.payment_mode=undefined;
    this.rateList.wh_storage_location_name=undefined;
    this.rateList.transit_type_name=undefined;
    this.rateList.shipment_type=undefined;
    this.rateList.minimum_wgt=undefined;
    this.rateList.min_rate_per_shipment=undefined;
    this.rateList.transit_days_to_delivery=undefined;
    this.rateList.fuel_sur_charge=undefined;
  }
  reset_slap(){
    this.slapList.slap_weight=undefined,
    this.slapList.slap_amount=undefined,
    this.slapList.slap_rate_type_UOM=undefined
  }

  ngOnInit() {
  	this.branchList();
  	this.destination();
  	this.getPayment();
  	this.getService();
  	this.getShipment();
    this.getRate();
  }

}
