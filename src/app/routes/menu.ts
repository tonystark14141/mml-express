
const Home = {
    text: 'Home',
    link: '/home',
    icon: 'icon-home'
};

const Dashboard = {
    text: 'Dashboard',
    link: '/dashboard/v1',
    icon: 'icon-speedometer',

};

const Masters = {
    text: 'Masters',
    link: '/masters',
    icon: 'icon-home'
};
const Reports = {
    text: 'Reports',
    // link: '/dashboard/v1',
    icon: 'icon-speedometer',
    submenu: [
        {
             text: 'Bill Generation',
             link: '/bill-generation',
        },
        {
            text: 'Bill View',
            link: '/bill-view',
        },
        {
            text: 'Invoice Issuing',
            link: '/invoice-issuing',
        },
         {
            text: 'Without Sale Entry',
            link: '/without-sale-entry',
        }
    ]
};

export const menu = [
  
    // Home,
    Dashboard,
    Masters,
    Reports,
   
];
