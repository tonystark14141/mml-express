import { Component, OnInit } from '@angular/core';
import { GenerateBillServiceService } from "../../../services/generate-bill-service.service";
import { HttpClient } from '@angular/common/http';
import {ActivatedRoute,Router} from '@angular/router';
@Component({
  selector: 'app-bill-generation',
  templateUrl: './bill-generation.component.html',
  styleUrls: ['./bill-generation.component.scss']
})
export class BillGenerationComponent implements OnInit {
branch:any;
customer:any;
payment:any;
from_date:any;
to_date:any;
invoice_date:any;
datas:any;
branchlist:any;
customerlist:any;
paymentlist:any;
report:any;

  constructor(public service:GenerateBillServiceService) { }

branch_list()
{
this.service.get_branchs().subscribe(params=>{

          this.branchlist=params.data;
          console.log('branchlist', this.branchlist)
});
}

customer_list()
{

this.service.get_customer(this.branch).subscribe(params=>{

          this.customerlist=params.data;
          console.log('customerlist', this.customerlist)
});
}

payment_list()
{
this.service.get_payment().subscribe(params=>{

          this.paymentlist=params.data;
          console.log('paymentlist', this.paymentlist)
});
}
submit()
{
	this.datas={
		branch:this.branch,
		customer:this.customer,
		payment:this.payment,
		from_date:this.from_date,
		to_date:this.to_date,
		invoice_date:this.invoice_date
	}
  console.log('cus', this.datas)
	  	this.service.getbill_report(this.datas).subscribe(param=>{
  		this.report=param['data'];
  		console.log("refe", this.report);
  		

  	});
}
reset()
{
	this.branch="",
	this.customer="",
	this.payment="",
	this.from_date="",
	this.to_date="",
	this.invoice_date=""
}
  ngOnInit() {
  	this.branch_list();
  	// this.customer_list();
  	this.payment_list();
  	this.reset();
  }

}
