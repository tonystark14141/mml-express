import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { BillService } from '../../../services/bill.service';

@Component({
  selector: 'app-bill-view',
  templateUrl: './bill-view.component.html',
  styleUrls: ['./bill-view.component.scss'],
})
export class BillViewComponent implements OnInit {
data:any;
data1:any;
data2:any;
data3:any;
datas:any;
origin_id:any;
to_date:any;
from_date:any;
origin_client_id:any;
datas2:any;
  constructor(public bill:BillService,private http:HttpClient) { }
    getbranch(){
  	this.bill.getBranch().subscribe(params=>{
      this.data=params['data'];
console.log("list",this.data);
  })
}
  getcus(origin_client_id){
  	this.bill.getCus(origin_client_id).subscribe(params=>{
      this.data1=params['data'];
console.log("list1",this.data1);

  })
}
print(datas2) {
      this.bill.getPdf(datas2).subscribe(params=>{
      this.data3=params;
console.log("list3",this.data3);
window.open(this.data3.file_url)
      })
    }
  
  searchh(datas){
  	 this.datas={
  		origin_id:this.origin_id,
  		origin_client_id:this.origin_client_id,
  		to_date:this.to_date, 
  		from_date:this.from_date,
  	}
  	this.bill.getTab(this.datas).subscribe(params=>{
      this.data2=params['data'];
console.log("list2",this.data2);

  })
}
resett(){
	this.origin_id='',
	this.origin_client_id='',
    this.to_date='',
    this.from_date=''
    // this.searchh(this.datas);
}


  ngOnInit() {
  	this.getbranch();
    this.resett();
  }
}