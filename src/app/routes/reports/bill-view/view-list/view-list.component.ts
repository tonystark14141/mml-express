import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { BillService } from '../../../../services/bill.service';
import { Router,ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-view-list',
  templateUrl: './view-list.component.html',
  styleUrls: ['./view-list.component.scss']
})
export class ViewListComponent implements OnInit {
data3:any;
data2:any;
datas:any;
hawb_billingInvoice_no:any;
  constructor(public bill:BillService,private http:HttpClient,public route:ActivatedRoute) { 
this.route.params.subscribe(data=>{
      this.hawb_billingInvoice_no=data['id'];
      console.log('id',this.hawb_billingInvoice_no)
    })
  }

getlist(hawb_billingInvoice_no){
    this.bill.getList(this.hawb_billingInvoice_no).subscribe(params=>{
      this.data3=params['data'];
console.log("list3",this.data3);

  })
}
  ngOnInit() {
  	this.getlist(this.hawb_billingInvoice_no);
  }
}

