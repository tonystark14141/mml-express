import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WithoutSaleEntryComponent } from './without-sale-entry.component';

describe('WithoutSaleEntryComponent', () => {
  let component: WithoutSaleEntryComponent;
  let fixture: ComponentFixture<WithoutSaleEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WithoutSaleEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WithoutSaleEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
