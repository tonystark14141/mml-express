import { Component, OnInit } from '@angular/core';
import { SaleServiceService } from "../../../services/sale-service.service";
import { HttpClient } from '@angular/common/http';
import {ActivatedRoute,Router} from '@angular/router';
@Component({
  selector: 'app-without-sale-entry',
  templateUrl: './without-sale-entry.component.html',
  styleUrls: ['./without-sale-entry.component.scss']
})
export class WithoutSaleEntryComponent implements OnInit {

  constructor(public service:SaleServiceService) { }
origin:any;
from_date:any;
to_date:any;
datas:any;
originlist:any;
report:any;
origin_list()
{
this.service.get_origin().subscribe(params=>{
	console.log(params)
          this.originlist=params.data;
          console.log('origin', this.originlist)
});
}
submit()
{
	console.log(this.origin,this.from_date,this.to_date);
	this.datas={
		origin:this.origin,
		from_date:this.from_date,
		to_date:this.to_date
	}
	  	this.service.getsale_report(this.datas).
	  	subscribe(param=>{
  		this.report=param;
  		console.log("refe", this.report);
  		

  	})
}
reset()
{
	this.origin="",
	this.from_date="",
	this.to_date=""
}
  ngOnInit() {
  	this.origin_list();
  	this.reset();
  }

}
