import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslatorService } from '../core/translator/translator.service';
import { MenuService } from '../core/menu/menu.service';
import { SharedModule } from '../shared/shared.module';
import { PagesModule } from './pages/pages.module';

import { menu } from './menu';
import { routes } from './routes';
import { MastersComponent } from './masters/masters.component';
import { BillGenerationComponent } from './reports/bill-generation/bill-generation.component';
import { BillViewComponent } from './reports/bill-view/bill-view.component';
import { InvoiceIssuingComponent } from './reports/invoice-issuing/invoice-issuing.component';
import { WithoutSaleEntryComponent } from './reports/without-sale-entry/without-sale-entry.component';
import { ViewListComponent } from './reports/bill-view/view-list/view-list.component';

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forRoot(routes),
        PagesModule
    ],
    declarations: [MastersComponent, BillGenerationComponent, BillViewComponent, InvoiceIssuingComponent, WithoutSaleEntryComponent, ViewListComponent],
    exports: [
        RouterModule
    ]
})

export class RoutesModule {
    constructor(public menuService: MenuService, tr: TranslatorService) {
        menuService.addMenu(menu);
    }
}
