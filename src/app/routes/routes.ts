import { LayoutComponent } from '../layout/layout.component';

import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { RecoverComponent } from './pages/recover/recover.component';
import { LockComponent } from './pages/lock/lock.component';
import { MaintenanceComponent } from './pages/maintenance/maintenance.component';
import { Error404Component } from './pages/error404/error404.component';
import { Error500Component } from './pages/error500/error500.component';
import { MastersComponent } from './masters/masters.component';
import { BillGenerationComponent } from './reports/bill-generation/bill-generation.component'
import { BillViewComponent } from './reports/bill-view/bill-view.component';
import { InvoiceIssuingComponent } from './reports/invoice-issuing/invoice-issuing.component';
import { WithoutSaleEntryComponent } from './reports/without-sale-entry/without-sale-entry.component';
import { ViewListComponent } from './reports/bill-view/view-list/view-list.component';
export const routes = [

    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard/v1', pathMatch: 'full' },
            { path: 'home', loadChildren: './home/home.module#HomeModule' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'masters', component:MastersComponent},
            { path: 'bill-generation', component:BillGenerationComponent },
            { path: 'bill-view', component:BillViewComponent},
            { path: 'invoice-issuing', component:InvoiceIssuingComponent},
            { path: 'without-sale-entry', component:WithoutSaleEntryComponent},
            { path: 'view-list/:id', component:ViewListComponent }

        ]
    },

    // Not lazy-loaded routes
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'recover', component: RecoverComponent },
    { path: 'lock', component: LockComponent },
    { path: 'maintenance', component: MaintenanceComponent },
    { path: '404', component: Error404Component },
    { path: '500', component: Error500Component },

    // Not found
    { path: '**', redirectTo: 'dashboard' }

];
