import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import  { retry,catchError } from 'rxjs/operators';
import { Constant } from '../constants/constant';
@Injectable({
  providedIn: 'root'
})
export class BillService {

  constructor(private http: HttpClient ) { }
     httpOptions={
	headers:new HttpHeaders({
		'content-type':'application'
	})
}
getBranch():Observable<any> {
  return this.http.get(Constant.CONSTANT_API + 'read/point_origin?id=').pipe(retry(2))
}
getCus(origin_client_id):Observable<any> {
  return this.http.get(Constant.CONSTANT_API + 'read/customer_by_origin?origin_id='
  	+origin_client_id+'&customer_id=-1').pipe(retry(2))
}
getTab(datas):Observable<any> {
  return this.http.get(Constant.CONSTANT_API + 'read/invoice_bill_list?origin_id='+
  	datas.origin_id+'&customer='+datas.origin_client_id+'&from_date='+
  	datas.from_date+'&to_date='+datas.to_date).pipe(retry(2))
}
getPdf(datas2):Observable<any> {
  return this.http.get(Constant.CONSTANT_API + 'download/generate_domestic_bill?bill_no='
    +datas2.hawb_billingInvoice_no+'&customer_acc='+datas2.client_id+
    '&invoice_base=&current_date='+datas2.billing_date+
    '&origin_branch_id=&print_logo=true&print_address=true&paymentTypeDisplayName='
    +datas2.payment_mode).pipe(retry(2))
}
getList(hawb_billingInvoice_no):Observable<any>{
  return this.http.get(Constant.CONSTANT_API + 'read/invoice_bill_by_no?id=&bill_no='
    +hawb_billingInvoice_no+'&customer_id=&from_date=&to_date=').pipe(retry(2))
}
}
	