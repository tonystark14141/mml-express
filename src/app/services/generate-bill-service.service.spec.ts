import { TestBed } from '@angular/core/testing';

import { GenerateBillServiceService } from './generate-bill-service.service';

describe('GenerateBillServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GenerateBillServiceService = TestBed.get(GenerateBillServiceService);
    expect(service).toBeTruthy();
  });
});
