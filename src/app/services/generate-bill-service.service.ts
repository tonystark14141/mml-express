import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Constant } from '../constants/constant';
import 'rxjs/add/operator/catch';

@Injectable({
  providedIn: 'root'
})
export class GenerateBillServiceService {
url:any;
retry:any;
  constructor(private http: HttpClient) { }
    httpOptions={

  	headers:new HttpHeaders({
  		'content-type':'application/json'
  	})
  }
     get_branchs():Observable<any>{
    return this.http.get(Constant.CONSTANT_API + "read/point_origin?id=").pipe(retry(2))
  }
       get_customer(org_id):Observable<any>{
    return this.http.get(Constant.CONSTANT_API + "read/customer_by_origin?origin_id=" + org_id +"&customer_id=-1").pipe(retry(2))
  }
       get_payment():Observable<any>{
    return this.http.get(Constant.CONSTANT_API + "read/payment_modes").pipe(retry(2))
  }
    getbill_report(datas):Observable<any>{
  	return this.http.get(Constant.CONSTANT_API + "read/bill_invoice_list?customer_id="+datas.customer
  	+"&from_date="+datas.from_date +"&to_date="+datas.to_date +"&payment_mode="
  	+datas.payment+ "&invoice_id=&invoice_no=").pipe(retry(2))
  }
}
