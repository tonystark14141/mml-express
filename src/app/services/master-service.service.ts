import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse  } from '@angular/common/http';
import { Observable , throwError} from 'rxjs';
import { retry , catchError} from 'rxjs/operators';
import { Constant } from '../constants/constant'

@Injectable({
  providedIn: 'root'
})
export class MasterServiceService {

  constructor(private http:HttpClient) { }
  httpOptions={
	headers:new HttpHeaders({'content-type' : 'applicaton/json'})
}
get_branch():Observable<any>{
	return this.http.get(Constant.CONSTANT_API + "read/point_origin?id=").pipe(retry(2));
}
get_customer(id):Observable<any>{
	return this.http.get(Constant.CONSTANT_API + "read/customer_by_origin?origin_id="+id +"&customer_id=-1").pipe(retry(2));
}
get_tariff(id):Observable<any>{
	return this.http.get(Constant.CONSTANT_API +"read/customer_rate?id=-1&customer_id="+id).pipe(retry(2));
}
get_destination():Observable<any>{
	return this.http.get(Constant.CONSTANT_API +"read/delivery_locations").pipe(retry(2));
}
get_payment():Observable<any>{
	return this.http.get(Constant.CONSTANT_API +"read/payment_modes").pipe(retry(2));
}
get_shipment_type():Observable<any>{
	return this.http.get(Constant.CONSTANT_API + "read/shipment_type").pipe(retry(2));
}
get_service():Observable<any>{
	return this.http.get(Constant.CONSTANT_API +"read/transit_type").pipe(retry(2));
}
get_rate():Observable<any>{
	return this.http.get(Constant.CONSTANT_API +"read/unit_measure_types").pipe(retry(2));
}
get_rate_id(id):Observable<any>{
	return this.http.get(Constant.CONSTANT_API +"read/customer_rate?id="+id+"&customer_id=-1").pipe(retry(2))
}
get_slap_id(id):Observable<any>{
	return this.http.get(Constant.CONSTANT_API +"read/rate_slap?id=-1&master_id="+id).pipe(retry(2));
}
get_slap_id_get(id):Observable<any>{
	return this.http.get(Constant.CONSTANT_API +"read/rate_slap?id="+id+"&master_id=-1").pipe(retry(2));
}
insert(data):Observable<any>{
	return this.http.post(Constant.CONSTANT_API +"create/insert_transaction",JSON.stringify(data),this.httpOptions).pipe(retry(2))
}
update(data):Observable<any>{
	return this.http.post(Constant.CONSTANT_API +"update/transaction",JSON.stringify(data),this.httpOptions).pipe(retry(2));
}
insert_slap(data):Observable<any>{
	return this.http.post(Constant.CONSTANT_API +"create/insert_transaction_slap",JSON.stringify(data),this.httpOptions).pipe(retry(2))
}
update_slap(data):Observable<any>{
	return this.http.post(Constant.CONSTANT_API +"update/rate_slap",JSON.stringify(data),this.httpOptions).pipe(retry(2));
}
delete_slap(id):Observable<any>{
	return this.http.get(Constant.CONSTANT_API +"delete/rate_slap?id="+id).pipe(retry(2))
}
delete_tariff(id):Observable<any>
{
	return this.http.get(Constant.CONSTANT_API +"delete/transaction?id="+id).pipe(retry(2));
}
print(id):Observable<any>{
	return this.http.get(Constant.CONSTANT_API +"download/generate_tariff_list?customer_id="+id).pipe(retry(2));
}
}
